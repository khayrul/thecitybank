<?php


use Illuminate\Database\Seeder as SeederAlias;

use Database\Seeders\UserSeeder;

class DatabaseSeeder extends SeederAlias
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UserSeeder::class);
    }
}
