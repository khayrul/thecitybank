<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Mr User';
        $user->email = 'user@example.com';
        $user->type = 1;
        $user->password = bcrypt('password');
        $user->save();


    }
}
