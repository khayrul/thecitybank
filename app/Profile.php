<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Profile extends Model
{
    public $table = 'profiles';
    protected $fillable = [
        'fname','lname','email','mobile','position','team','nid','qualification'
    ];

    protected $hidden = ['email'];

    public function userModel()
    {
        return $this->hasOne('App\User','id','user_id');
    }

}
