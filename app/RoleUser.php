<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use App\User;

class RoleUser extends Model
{
    public $table = 'role_user';
    protected $fillable = [
        'user_id','role_id'
    ];

    protected $hidden = ['email'];

    public function userModel()
    {
        return $this->hasOne('App\User','id','user_id');
    }

}
