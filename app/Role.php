<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Role extends Model
{

    protected $fillable = [
        'title'
    ];

    protected $hidden = ['email'];

    public function userModel()
    {
        return $this->hasOne('App\User','id','user_id');
    }

}
